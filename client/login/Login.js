Template.Login.onCreated(function(){
  this.loginMode = new ReactiveVar(true);
});

Template.Login.events({
  "click #at-signUp": function(event, template){
    event.preventDefault();
    template.loginMode.set(false);
  },
  "click #at-signIn": function (event, template){
    event.preventDefault();
    template.loginMode.set(true);
  }

});
Template.Login.helpers({
  loginMode: function(){
    return Template.instance().loginMode.get();
  }
});
