Template.LoginForm.onCreated(function() {
    this.error = new ReactiveVar(false);
});

Template.LoginForm.events({
    'submit form': function(event, template){
        event.preventDefault();
        var emailVar = event.target.loginEmail.value;
        var passwordVar = event.target.loginPassword.value;
        Meteor.loginWithPassword(
          emailVar,
          passwordVar,
          function(error) {
            if (error) {
              console.log(error);
              template.error.set(true);
            }
          });
    }
});

Template.LoginForm.helpers({
  error: function() {
    return Template.instance().error.get();
  }
});
