Template.SignUpForm.onCreated(function() {
    this.error = new ReactiveVar("");
});

Template.SignUpForm.events({
    'submit form': function(event, template) {
      console.log("click");
        event.preventDefault();
        var emailVar = event.target.signupEmail.value;
        var passwordVar = event.target.signupPassword.value;
        var nameVar = event.target.signupName.value;

        if (validateEmail(emailVar)) {
          Meteor.call('registerUser', emailVar, passwordVar, nameVar, function(error) {
            if(error){
              template.error.set(error.reason);
            }
            else{
              Meteor.loginWithPassword(emailVar, passwordVar, function(error) {
                if(error) {
                  template.error.set(error.reason);
                }
              });
            }
          });
        }
        else {
          template.error.set("Please enter a valid email");
        }
    }
});

function validateEmail(email) {
  var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
  return re.test(email);
}


Template.SignUpForm.helpers({
  error: function() {
    console.log(Template.instance().error.get())
    return Template.instance().error.get();
  }
});
