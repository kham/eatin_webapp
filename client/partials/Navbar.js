Template.Navbar.helpers({
  getUser: function() {
    return Meteor.user().profile.name;
  }
});

Template.Navbar.events({
  'click .logout': function(event) {
    event.preventDefault();
    Meteor.logout(function(err) {
      FlowRouter.go('/');
    });
  }
});
