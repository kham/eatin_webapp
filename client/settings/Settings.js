Template.Settings.onCreated(function() {
    var self = this;
});

AutoForm.addHooks(['insertListForm'], {
     onSuccess: function (operation, result, template) {
       FlowRouter.go("/lists");
     }
});

Template.Settings.events({
  'submit form': function(event, template){
    var emailVar = event.target.emailUser.value;
    var nameVar = event.target.userName.value;

    Meteor.call('updateUserInfo', emailVar, nameVar);
  }
});

Template.Settings.helpers({
  getProfile: function() {
    console.log(Meteor.user());
    return Meteor.user();
  }
});
