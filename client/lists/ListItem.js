Template.ListItem.onCreated(function() {
  var self = this;
  self.autorun(function() {
    self.subscribe('user');
  });
  this.editMode = new ReactiveVar(false);
  this.shareMode = new ReactiveVar(false);
  this.error = new ReactiveVar("");
  this.success = new ReactiveVar("");
}) ;

AutoForm.addHooks(['updateListId'], {
     onSuccess: function (operation, result) {
       FlowRouter.go("/");
     }
});

Template.registerHelper('equals', function (a, b) {
  return a === b;
});

Template.ListItem.events({
  'click #removeListItem': function(event, template) {
    console.log("delete");
    Meteor.call('deleteListItem', this._id);
    $('#modalDelete').modal('hide');
    $('.modal-backdrop').remove();
    $('body').removeClass('modal-open');
    $('body').css('padding-right','0');
    template.shareMode.set(false);
  },
  'click #unsubscribeListItem': function(event) {
    console.log("unsubscribe");
    Meteor.call('updateListSubscribers', this._id);
    $('#modalDelete').modal('hide');
    $('.modal-backdrop').remove();
    $('body').removeClass('modal-open');
    $('body').css('padding-right', '0');
  },
  'click #updateListItem': function(event, template) {
    console.log("modify");
    template.editMode.set(!template.editMode.get());
    template.shareMode.set(false);
  },
  'click #shareListItem': function(event, template) {
    console.log("share");
    template.shareMode.set(!template.shareMode.get());
    template.success.set("");
    template.error.set("");
  },
  'submit #shareUserForm': function(event, template) {
    console.log("submit form");
      event.preventDefault();
      var emailVar = event.target.shareUserEmail.value;
      var user = Meteor.users.findOne({ "emails.address" : emailVar });
      if (user != null) {
        Meteor.call('shareListItem', this._id, emailVar);
        template.success.set("User added successfully");
        template.error.set("");
      }
      else {
        template.success.set("");
        template.error.set("This user doesn't exist");
      }
  },
  'click #closeUpdate': function(event, template) {
    template.editMode.set(!template.editMode.get());
    template.shareMode.set(false);
  }
});

Template.ListItem.helpers({
  isError: function() {
    return Template.instance().error.get() != "";
  },
  isSuccess: function() {
    return Template.instance().success.get() != "";
  },
  getError: function() {
      return Template.instance().error.get();
  },
  getSuccess: function() {
      return Template.instance().success.get();
  },
  getEditMode: function() {
    return Template.instance().editMode.get();
  },
  getShareMode: function() {
    return Template.instance().shareMode.get();
  },
  getAuthor: function() {
    return Meteor.users.findOne({_id:this.author});
  },
  getCurrentUserId : function() {
    return Meteor.userId();
  }
});
