Template.ListDetail.onCreated(function() {
    var self = this;
    self.autorun(function() {
      var id = FlowRouter.getParam('id');
      self.subscribe('listDetail', id);
      self.subscribe('user');
    });
});

Template.ListDetail.events({
  'click #addRestaurant': function() {
  var id = FlowRouter.getParam('id');
  var link = '/list/'.concat(id).concat('/newRestaurant');
  console.log(link);
  FlowRouter.go(link);
  }
});

Template.ListDetail.helpers({
  list: function() {
    var id = FlowRouter.getParam('id');
    return Lists.findOne({_id:id});
  },
  getId: function() {
    var id = FlowRouter.getParam('id');
    return id;
  },
  getAuthor: function() {
    var id = FlowRouter.getParam('id');
    var list = Lists.findOne({_id:id});
    return Meteor.users.findOne({_id:list.author});
  }
});
