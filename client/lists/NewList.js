Template.Lists.onCreated(function() {
    var self = this;
    self.autorun(function() {
      self.subscribe('lists');
    });
});

AutoForm.addHooks(['insertListForm'], {
     onSuccess: function (operation, result, template) {
       FlowRouter.go("/lists");
     }
});

Template.Lists.helpers({
  lists: ()=> {
    return Lists.find({});
  }
});
