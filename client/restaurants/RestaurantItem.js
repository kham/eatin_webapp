Template.RestaurantItem.onCreated(function() {
    var self = this;
    self.autorun(function() {
      var id = FlowRouter.getParam('id');
      self.subscribe('listDetail', id);
    });
    this.editMode = new ReactiveVar(false);
});

Template.RestaurantItem.events({
  'click #removeRestaurantItem': function() {
    console.log("delete");
    Meteor.call('deleteRestaurantItem', id, this._id);
  },
  'click #updateRestaurantItem': function(event, template) {
    console.log("modify");
    template.editMode.set(!template.editMode.get());
  },
  'click #closeUpdate': function(event, template) {
    template.editMode.set(!template.editMode.get());
  }
});

Template.RestaurantItem.helpers({
  getEditMode: function() {
    return Template.instance().editMode.get();
  }
});
