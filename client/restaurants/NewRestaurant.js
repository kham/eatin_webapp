Template.NewRestaurant.onCreated(function() {
    var self = this;
    self.autorun(function() {
      var id = FlowRouter.getParam('id');
      self.subscribe('listDetail', id);
      console.log(id);

    });
});

AutoForm.addHooks(['insertRestaurantForm'], {
     onSuccess: function (operation, result, template) {
       var id = FlowRouter.getParam('id');
       FlowRouter.go("/list/".concat(id));
     }
});

Template.NewRestaurant.helpers({
  restaurantDoc : function() {
    var id = FlowRouter.getParam('id');
    return Lists.findOne({_id:id});
  }
});
