Lists = new Mongo.Collection('lists');

Lists.allow({
  insert: function(userId, doc) {
    return !!userId;
  },
  update: function(userId, doc) {
    return !!userId;
  }
});

Restaurant = new SimpleSchema({
  restoName: {
    type: String,
    label: 'Name'
  },
  restoAddress: {
    type: String,
    label: 'Address'
  },
  restoType: {
    type: String,
    label: 'Type of Food',
    allowedValues: ["Chinese", "French", "Italian", "Japanese", "Mexican"]
  },
  restoDesc: {
    type: String,
    label: 'Description'
  }
})

ListSchema = new SimpleSchema({
  name: {
    type: String,
    label:'Name'
  },
  desc: {
    type: String,
    label: "Description"
  },
  restaurants : {
    type:[Restaurant],
    optional: true
  },
  subscribers : {
    type: [String],
    optional:true
  },
  author: {
    type: String,
    label: 'Author',
    autoValue:function() {
        if (this.isInsert) {
          return this.userId;
        } else if (this.isUpsert) {
          return {$setOnInsert: this.userId};
        } else {
          this.unset();  // Prevent user from supplying their own value
        }
    },
    autoform: {
      type: "hidden"
    }
  },
  createdAt: {
    type: Date,
    label : "Created At",
    autoValue: function() {
        if (this.isInsert) {
          return new Date();
        } else if (this.isUpsert) {
          return {$setOnInsert: new Date()};
        } else {
          this.unset();  // Prevent user from supplying their own value
        }
    },
    autoform: {
      type: "hidden"
    }
  }
});

Lists.attachSchema(ListSchema);

Meteor.methods({
  deleteListItem : function(id) {
    Lists.remove(id);
  },
  deleteRestaurantItem : function(id,idRestaurant) {
    Lists.update(
      {_id:idRestaurant},
      {$pull:{restaurants:idRestaurant}}
    );
  },
  shareListItem : function(listId, email) {
    var userId = Meteor.users.findOne({"emails.address": email})._id;
    Lists.update(
      {_id:listId},
      {$push:{subscribers:userId}}
    );
  },
  registerUser : function(emailVar, passwordVar, nameVar) {
      Accounts.createUser({
        email: emailVar,
        password: passwordVar,
        profile: {
          name: nameVar
        }
    });
  },
   updateUserInfo: function(emailVar, nameVar) {
     Meteor.users.update(
       {_id:this.userId},
       {$set:{"profile.name":nameVar, "emails.0.address":emailVar}}
     );
   },
   updateListSubscribers: function(listId) {
     Lists.update(
       {_id:listId},
       {$pull:{subscribers:this.userId}}
     );
   }
});
