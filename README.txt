Welcome to Eatin. 

It's time for lunch and you don't know where to eat?

The purpose of this web app is to manage and share list of restaurants with friends, coworkers or other users.

-------------------------
-	USER MANUAL	-
------------------------- 
- If you haven't created an account yet, start by filling the form after clicking the "Register" link.
- You will be able to login with your email/password later after logging out.
- After creating an account, you will be redirected to the dashboard.

On this dashboard, you can : 
1. add a new list 
2. manage the created list
3. access to the settings by clicking on the right dropdown in the navigation bar
4. log out by clicking on the right dropdown in the navigation bar


1. ADD A NEW LIST
When clicking on 'Add a new List' on the navigation bar, you will be redirected to a form to create a new list

You will have to fill the name and the description of the list.

After clicking to the button 'Submit', the list will be stored in the Mongo database and you will be redirected to your list of list.

2. MANAGE THE CREATED LIST
There are two types of list in the created list :
- the one you possess after creating it (you are the author of the list)
- the one that belongs to other user and that have been shared to you.

When you are an author of a list, you can :
2.1. delete the list (when clicking on the cross icon)
2.2. update the name & the description (when clicking on the pencil icon)
2.3. share to other user (when clicking on the share icon)

When you do not own the list but someone shared the list with you, you can :
2.4. unsubscribe to the list

In both case, you can :
2.5. View Details

2.1. DELETE THE LIST
A popup will appear and ask you to confirm the deletion. After clicking on 'Confirm', the list will be removedfrom the database.

2.2. UPDATE THE NAME & THE DESCRIPTION
A form will appear in the card and you will be able to update the fields name & description of the list.

2.3. SHARE TO OTHER USER
A form will appear in the card and you will be able to add other user to the list and share it with them

2.4. UNSUBSCRIBE TO THE LIST
A popup will appear and ask you to confirm the unsubscription. After clicking on 'Confirm', the current user will be remove frome the list of subscribers.


2.5. VIEW DETAILS
When clicking on 'View Details', you will be redirected to the detail of the list.
On this page, you will be able to add restaurants to the list. 

When clicking on 'ADD A RESTAURANT', you will be redirected to a form to fill. This will add the restaurant to the list and a card of the restaurant will appear on the Detail Page.

3. ACCESS TO THE SETTINGS
you can update the email & name when clicking on the settings

4. LOG OUT
You will be logged out and will be redirected to the Homepage




