if (Meteor.isClient) {
  Accounts.onLogin(function() {
    FlowRouter.go('lists');
  });

  Accounts.onLogout(function() {
    FlowRouter.go('home');
  });
}


FlowRouter.triggers.enter([function(context, redirect){
  if (!Meteor.userId()) {
    FlowRouter.go('home');
  }
}]);

FlowRouter.route('/', {
  name:'home',
  action() {
    if (Meteor.userId()) {
      FlowRouter.go('lists');
    }
    BlazeLayout.render('HomeLayout');
  }
});

FlowRouter.route('/newList', {
  name:'newList',
  action() {
    BlazeLayout.render('MainLayout', {main:'NewList'});
    $(".nav").find(".active").removeClass("active");
    $("#newListMenu").addClass("active");
  }
});

FlowRouter.route('/lists', {
  name:'lists',
  action() {
    BlazeLayout.render('MainLayout', {main:'Lists'});
    $(".nav").find(".active").removeClass("active");
    $("#listMenu").addClass("active");
  }
});

FlowRouter.route('/list/:id', {
  name:'',
  action() {
    BlazeLayout.render('MainLayout', {main:'ListDetail'});
    $(".nav").find(".active").removeClass("active");
    $("#listMenu").addClass("active");
  }
});

FlowRouter.route('/list/:id/newRestaurant', {
  name:'',
  action() {
    BlazeLayout.render('MainLayout', {main:'NewRestaurant'});
    $(".nav").find(".active").removeClass("active");
    $("#listMenu").addClass("active");
  }
});

FlowRouter.route('/settings', {
  name:'',
  action() {
    BlazeLayout.render('MainLayout', {main:'Settings'});
    $(".nav").find(".active").removeClass("active");
  }
});
