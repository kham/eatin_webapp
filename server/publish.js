Meteor.publish('user', function() {
  return Meteor.users.find();
});

Meteor.publish('lists', function() {
  return Lists.find({
     $or: [
       {author: this.userId},
       {subscribers:{$in:[this.userId]}}
     ]
   });
});

Meteor.publish('listDetail', function(id) {
  check(id, String);
  return Lists.find({_id: id})
});
